/**
 * Extract user details from cookie
 * Cookie are stored as raw String in the request
 * @param req HTTPRequest
 */
exports.extractUserFromRequest = (req) => {
  if (!req.headers['cookie'] || !req.headers['cookie'].includes('salt')) {
    return false;
  }
  const cookie = req.headers['cookie']
  const id = cookie.substring(cookie.indexOf(' steamid=') + 9,
    cookie.indexOf(' steamid=') + 26).replace(';', '')
  const salt = cookie.substring(cookie.indexOf(' salt=') + 6,
    cookie.indexOf(' salt=') + 15).replace(';', '')
  return {
    id: id,
    salt: salt
  }
}
