const io = require('./client')()
const auth = require('../auth/auth')
const cookies = require('../cookies/cookies')
const conf = require('../../conf/config')
const logger = require('../../conf/logger')
const userService = require('../services/user.service')
const roomService = require('../services/room.service')


const steamIdList = [];
const socketIdList = [];

/**
 * discord
 */
const emitOnlineCount = () => {
  let ids = [];
  for (var i = 0; i < steamIdList.length; i++) {
    if (!ids.includes(steamIdList[i])) {
      ids.push(steamIdList[i]);
    }
  }
  // discordbot.emitOnlineCount(ids.length, db.GetCounts());
}


/**
 *
 * @param roomId : string
 */
const emitOnlineUsers = (roomId) => {
  const users = [];
  io.in(roomId)
    .clients((error, clients) => {
      for (var i = 0; i < clients.length; i++) {
        users.push(steamIdList[socketIdList.indexOf(clients[i])])
      }
      const roomUsers = roomService.findUsersInRoom(roomId)
      const packet = {
        users: roomUsers,
        onlineusers: users
      };
      io.in(roomId)
        .emit('onlineusers', packet)
    });
}

/**
 *
 * @param socket : socket.io socket
 * @param roomId : string
 */
const disconnectUserHandler = (socket, roomId) => {
  const index = socketIdList.indexOf(socket.id);
  socketIdList.splice(index, 1);
  steamIdList.splice(index, 1);
  emitOnlineUsers(roomId);
  emitOnlineCount();
}

/**
 *
 * @param socket : socket.io socket
 * @returns {string}
 */
const getGlobalId = (socket) => {
  const prefix = conf.fhghq.url + '/'
  return socket.handshake.headers.referer.replace(prefix + 'room/', '');
}

/**
 *
 * @param users : []
 * @param roomId : string
 */
exports.updateusers = (users, roomId) => {
  //logger.info(users);
  io.in(roomId)
    .emit('updateusers', users)
//Updates users' status in their profiles
  users.forEach((user) => {
    const packet = {
      globalid: roomId,
      rank: user.rank
    }
    io.in(user.id)
      .emit('updateroom', packet);
    if (user.newrank === 5) {
      const index = steamIdList.indexOf(user.id);
      if (index !== -1) {
        io.sockets.connected[socketIdList[index]].disconnect();
      }
    }
  })
}

/**
 *
 * @param userId : string
 * @param roomId : string
 */
exports.emitNewAdmin = (userId, roomId) => {
  const packet = {
    globalid: roomId,
    rank: 1
  };
  io.in(userId)
    .emit('updateroom', packet); //if the new admin is in his lobby
  io.in(roomId)
    .emit('hailnewking', userId);
};


exports.start = () => {
  io.on('connection', (socket) => {
    const req = socket.handshake
    if (auth.isLoggedIn(req)) {
      const account = cookies.extractUserFromRequest(req)
      let link = req.headers.referer;
      socket.on('disconnect', () => { logger.info(`Disconnecting ${account.id}`)});

      if (link === conf.fhghq.url) {
        socket.join(account.id);
        logger.info(req.headers.referer);
        //logger.info("User "+socket.id+" ("+account.id+") joined room "+account.id);
        io.in(account.id)
          .emit('pong');
      } else if (link.includes(conf.fhghq.url + '/room/')) {
        // ? if we have a roomId, we set it inside the variable "link" =)
        link = link.replace(conf.fhghq.url + '/room/', '');
        let rank = userService.getUserRankInRoom(account.id, link)
        if (rank > 0 && rank < 5) {
          socket.join(link)

          steamIdList.push(account.id)
          socketIdList.push(socket.id)

          emitOnlineUsers(link)
          // emitOnlineCount()

          socket.on('disconnect', disconnectUserHandler.bind(null, socket, link))

          socket.on('updateusers', (users, steamid) => {
            let globalid = getGlobalId(socket)
            logger.info(`updating users for globalId ${globalid}`)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank < 3) {
              userService.updateUsersRankInRoom(steamid, users, globalid)
              exports.updateusers(users, globalid)
            }
          })

          socket.on('setRole', (user) => {
            //logger.info("updating users");
            const globalid = getGlobalId(socket)
            socket.broadcast.to(globalid)
              .emit('setRole', user)
            const newRole = JSON.stringify(user.role)
            userService.updateUserRoleInRoom(user.id, globalid, newRole)
          })

          socket.on('hailnewking', (adminid, steamid) => {
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank === 1) {
              userService.transferRoomOwnership(steamid, adminid, globalid)
              exports.emitNewAdmin(adminid, globalid)
            }
            const packet = {
              globalid: globalid,
              rank: 1
            };
            io.in(adminid)
              .emit('updateroom', packet)
          });

          socket.on('updateTech', (packet) => { //packet
            packet.globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(packet.globalid)
                .emit('updateTech', packet)
              packet.techtree = JSON.stringify(packet.techtree)
              roomService.updateRoomTech(packet.globalid, packet.techtree)
            }
          });

          socket.on('updateObject', (packet) => { //packet
            //logger.info(packet)
            packet.globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(packet.globalid)
                .emit('updateObject', packet)
              roomService.updateRoomObject(packet.globalid, packet.type, packet.object, packet.key)
            }
          });

          socket.on('deleteObject', (packet) => {
            //logger.info(packet);
            packet.globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(packet.globalid)
                .emit('deleteObject', packet)
              roomService.deleteRoomObject(packet.globalid, packet.type, packet.key)
            }
          })

          socket.on('addArtyResult', (packet) => { //packet
            packet.globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(packet.globalid)
                .emit('addArtyResult', packet)
              roomService.addArtyResult(packet.globalid, packet.totalstring);
            }
          })

          socket.on('updateSquads', (packet) => { //packet
            packet.globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(packet.globalid)
                .emit('updateSquads', packet);
              roomService.updateRoomSquads(packet.globalid, packet.data, packet.type)
            }
          })

          socket.on('submitEvent', (packet) => { //packet
            //logger.info(packet)
            packet.globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(packet.globalid)
                .emit('submitEvent', packet)
              roomService.addRoomEvent(packet.globalid, packet.type, packet.packet, packet.date)
            }
          })

          socket.on('submitOpTimer', (packet) => { //packet
            //logger.info(packet)
            packet.globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(packet.globalid)
                .emit('submitOpTimer', packet)
              roomService.submitOpTimer(packet.globalid, packet.date)
            }
          })

          socket.on('toggleSecure', (data) => { //packet
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank === 1 || rank === 2) {
              socket.broadcast.to(globalid)
                .emit('toggleSecure', data)
              if (data === 1) {
                const users = roomService.toggleRoomSecure(globalid, data)
                io.in(globalid)
                  .emit('updateusers', users)
              } else {
                roomService.toggleRoomSecure(globalid, data)
              }
            }
          })

          socket.on('clearRoom', () => { //packet
            //logger.info(packet)
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 3) {
              socket.broadcast.to(globalid)
                .emit('clearRoom')
              roomService.clearRoom(globalid)
            }
          })

          socket.on('clearMap', () => { //packet
            //logger.info(packet)
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 3) {
              socket.broadcast.to(globalid)
                .emit('clearMap')
              roomService.clearMap(globalid)
            }
          })

          socket.on('changeSettings', (type, data) => { //packet
            //logger.info(packet)
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 3) {
              //io.in(globalid).emit('updateroom',packet);
              socket.broadcast.to(globalid)
                .emit('changeSettings', type, data)
              roomService.changeSettings(globalid, type, data)
            }
          })

          socket.on('deleteSettings', (type) => { //packet
            //logger.info(packet)
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(globalid)
                .emit('deleteSettings', type)
              roomService.deleteSettings(globalid, type)
            }
          })

          socket.on('setDiscordToken', (token) => { //packet
            //logger.info(packet)
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 3) {
              //io.in(globalid).emit('updateroom',packet);
              socket.broadcast.to(globalid)
                .emit('setDiscordToken', token)
              // db.setDiscordToken(globalid, token)
            }
          })

          socket.on('disconnectDiscord', () => { //packet
            //logger.info(packet)
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 3) {
              socket.broadcast.to(globalid)
                .emit('disconnectDiscord')
              // db.disconnectDiscord(globalid)
            }
          })

          socket.on('addMessage', (packet, category) => { //packet
            //logger.info(packet)
            let globalid = getGlobalId(socket)
            rank = userService.getUserRankInRoom(account.id, link)
            if (rank > 0 && rank < 4) {
              socket.broadcast.to(globalid)
                .emit('addMessage', packet, category)
              roomService.addMessage(globalid, packet, category)
            }
          })

          socket.on('joinStats', () => { //packet
            socket.join('stats')
          })

          socket.on('leaveStats', () => {
            socket.leave('stats')
          })
        }
      }
    }
  })
}
