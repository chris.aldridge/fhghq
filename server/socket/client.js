
let io

/**
 * Singleton pattern allowing to share only 1 instance of socketIo client
 * @param http HTTPRequest
 * @returns {*}
 */
module.exports = (http) => {
  if (!io) io = require('socket.io')(http)
  return io
}
