const logger = require('../../conf/logger')
const warApiStaticRepository = require('../repository/warapi.static.repository')
const warApiClient = require('../warapi/warapi')

let regionNames = []
/**
 * regionName will be filled with static data from the api
 * once it the data is fetch there is not reason to call the api again
 * @returns A SINGLETON LIST
 */
exports.getRegionNames = () => {
  if (regionNames.length === 0) {
    return warApiClient.getMaps()
      .then(names => regionNames = names)
      .catch(error => logger.error(error))
  } else {
    return regionNames
  }
}

/**
 * Make stored static data usable by parsing string values
 * @returns {*}
 */
exports.list = () => {
  return warApiStaticRepository.list()
    .map((staticData) => {
      return {...staticData, ...{ data: JSON.parse(staticData.data)}}
    })
}

/**
 *
 * @param regionName : string
 */
exports.getEtag = (regionName) => {
  const dynamicData = warApiStaticRepository.getEtag(regionName)
  if (dynamicData) return dynamicData.result
  return undefined
}

exports.updateStaticData = async () => {
  const regionNames = await exports.getRegionNames()
  const regionUpdatesPromises = regionNames.map(async regionName => {
    const etag = exports.getEtag(regionName)
    const newStaticData = await warApiClient.getMapTextItems(regionName, etag)
    if (!newStaticData.data) {

    } else {
      const regionId = newStaticData.data.regionId
      warApiStaticRepository.insert(regionName, regionId, JSON.stringify(newStaticData.data), newStaticData.etag)
    }
  })
  await Promise.all(regionUpdatesPromises)
}
