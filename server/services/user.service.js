const roomRepository = require('../repository/rooms.repository')
const userRepository = require('../repository/user.repository')
const emitSocket = require('../socket/emit.socket')

/**
 * Get rank of a user in a room. A rank defines its permissions in the room
 * @param userId : string
 * @param roomId : string
 *
 * ? I dont really understand the checks and the magic values '7' and '8'
 *
 * @returns {number|*|number}
 */
exports.getUserRankInRoom = (userId, roomId) => {
  let room = roomRepository.findRoomById(roomId)
  if (!room) return 8
  room = roomRepository.getUserRankForRoom(userId, roomId)
  if (!room) return 7
  return room.rank
}

exports.getUserById = (userId) => {
  return userRepository.getUserById(userId)
}

exports.createUser = (user) => {
  return userRepository.createUser(user)
}

exports.updateUser = (user) => {
  return userRepository.updateUser(user)
}

/**
 *
 * @param userId : string
 * @returns {[]}
 */
exports.getUserRooms = (userId) => {
  const rooms = roomRepository.findRoomsByUserId(userId)
  const globalinfo = []
  if (rooms.length > 0) {
    for (let i = 0; i < rooms.length; i++) {
      const room = roomRepository.findRoomById(rooms[i].globalid)
      const settings = JSON.parse(room.settings)
      const object = {
        roomname: settings.name, admin: room.adminname, adminid: room.adminid, rank: rooms[i].rank, globalid: rooms[i].globalid,
      };
      globalinfo.push(object)
    }
  }
  return globalinfo
}

/**
 * Make a user leave a room
 * @param userId : string
 * @param roomId : string
 */
exports.leaveRoom = (userId, roomId) => {
  const room = roomRepository.findRoomById(roomId)
  if (userId !== room.adminid) {
    emitSocket.leaveRoom(userId, roomId)
  }
  rank = exports.getUserRankInRoom(userId, roomId)
  if (rank !== 6) {

    roomRepository.deleteUserRoomRelation(userId, roomId)
    // discordbot.leaveroom(id,globalid);
    /**
     * Delete room, should be a function somewhere else (room.service.js?)
     * ? rank == 1 must be "owner" or "Administrator" something like that
     */
    if (rank === 1) {
      roomRepository.deleteRoom(roomId)
      const users = roomRepository.findUsersInRoom(roomId)
      users.forEach(user => emitSocket.leaveRoom(user.id, roomId))
      // discordbot.deleteroom(id,globalid);
    }
  }

}

/**
 * ? Again a weird function
 * Get a user by ID and add the rank "5". "Requester" is actually named like this because
 * this specific user of rank 5 is used on users making request to acces a room.
 * @param userId
 * @returns {*}
 */
exports.getRequester = (userId) => {
  const user = userRepository.getUserById(userId)
  return {...user, ...{ rank: 5 }}
}

/**
 *
 * @param userId : string user asking to change ranks
 * @param users : [] with the new rank to apply
 * @param roomId : string
 */
exports.updateUsersRankInRoom = (userId, users, roomId) => {
  const userRank = exports.getUserRankInRoom(userId, roomId)
  if (userRank < 3) {
    users.forEach(userToUpdate => {
      const currentRank = exports.getUserRankInRoom(userToUpdate.id, roomId)
      if (currentRank !== 7 && currentRank !== 8) {
        const newRank = userToUpdate.rank
        if (newRank !== undefined && newRank < 6) {
          if ((userRank === 1 && newRank !== 1)
            || (userRank === 2 && newRank !==1 && currentRank !== 2 && newRank !== 2)) {
            userRepository.updateUserRankInRoom(userToUpdate.id, roomId, newRank)
          }
        }
      }
    })
  }
}


/**
 *
 * @param userId : string
 * @param roomId : string
 * @param role : string
 * @returns {*}
 */
exports.updateUserRoleInRoom = (userId, roomId, role) => {
  return userRepository.updateUserRoleInRoom(userId, roomId, role)
}

/**
 *
 * @param userId : string current admin user id doing the transfer
 * @param newAdminId : string userid to put as new admin
 * @param roomId : string
 * @returns {boolean}
 */
exports.transferRoomOwnership = function (userId, newAdminId, roomId) {
  const admin = (userId === roomRepository.findRoomById(roomId).adminid);
  const rank = exports.getUserRankInRoom(newAdminId, roomId);
  if (admin && (rank < 7)) {
    userRepository.updateUserRankInRoom(newAdminId, roomId, 1)
    userRepository.updateUserRankInRoom(userId, roomId, 2)
    roomRepository.setRoomAdmin(roomId, newAdminId)
    return true;
  }
  return false;
};
