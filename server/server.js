// server.js
const express = require('express');

// where your node app starts
const logger = require('../conf/logger')
const conf = require('../conf/config');

logger.info('Initializing database')
require('./repository/init')
logger.info('Database initialized')


// const discordbot = require('./discordbot.js'); //SHUTDOWN UNTIL FURTHER NOTICE

const app = express();
var http = require('http').Server(app);

// initialize socket client
require('./socket/client')(http) // initialize client
// initialize socket routes
require('./socket/routes.socket').start()

// initialize express controllers
require('./controllers/server.controller')(app)



const warApiDynamicDataService = require('./services/warapi.dynamic.service')
const statsService = require('./services/stats.service')

http.listen(3000, function () {
  logger.info('Your app is listening on port 3000')
});

/**
 * What's running in background
 * 1.
 *  1.1 Update statistics on startup
 *  1.2 Update map dynamic data
 * 2. Schedule the two updates
 */
statsService.updateStats()
  .catch((error) => logger.error(`Cloud not fetch stats: ${error}`))
warApiDynamicDataService.updateDynamicData()
  .catch((error) => logger.error(`Cloud not fetch dynamic data: ${error}`))
setInterval(statsService.updateStats, conf.warApi.statsUpdateInternal)
setInterval(warApiDynamicDataService.updateDynamicData, conf.warApi.mapUpdateInterval)
