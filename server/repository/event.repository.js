/**
 * Events repository
 */
const sqlClient = require('./client')()

/**
 * Look for all events in the db
 * Limited to the 400 latests events
 * @returns {*}
 */
exports.list = () => {
  return sqlClient.prepare('select * from events order by date desc limit 400').all()
}

/**
 *
 * @param regionId : string
 * @param date : string
 * @param prevItem : string
 * @param newItem : string
 * @returns {*}
 */
exports.insert = (regionId, date, prevItem, newItem) => {
  return sqlClient.prepare('INSERT INTO events (region, date, prevItem, newItem) VALUES (@region, @date, @prevItem, @newItem);')
    .run({
      region: regionId,
      date,
      prevItem,
      newItem
    })
}
