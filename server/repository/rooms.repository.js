/**
 * ? RoomsRepository
 */
const sqlClient = require('./client')()

/**
 * Get room list for given userId
 * @param userId : string
 */
exports.findRoomsByUserId = (userId) => {
  return sqlClient.prepare('SELECT * FROM userglobal WHERE userid = ?;').all(userId)
}

/**
 *
 * @param userId : string
 * @param roomId : string
 */
exports.getUserRankForRoom = (userId, roomId) => {
  return sqlClient.prepare('SELECT rank FROM userglobal WHERE userid = ? AND globalid = ?;')
    .get(userId, roomId)
}

/**
 * Return informations about a room
 * @param roomId : string
 * ? is it really roomId ?
 *
 * Apparently we are also sending additional room details such as admin user id and so on
 * @returns {{settings, adminname, adminid: (boolean|string|*)}|boolean}
 */
exports.findRoomById = function (roomId) {
  const result = exports.findRoomByIdSimple(roomId)
  if (!result) return false
  else {
    const adminname = sqlClient.prepare('SELECT * FROM users WHERE id = ?;').get(result.admin).name;
    return { adminname, adminid: result.admin, settings: result.settings, squads: result.squads, requests: result.requests };
  }
}


/**
 * ? Investigate the real use of fiondRoomById and findRoomByIdSimpe
 * looks like they pretty much to the same but we need to adapt the code using them (I think)
 * @param roomId
 * @returns {*}
 */
exports.findRoomByIdSimple = function (roomId) {
  return sqlClient.prepare('SELECT * FROM global WHERE id = ?;').get(roomId);
}

/**
 *
 * @param userId : string
 * @param roomId : string
 */
exports.deleteUserRoomRelation = (userId, roomId) => {
  return sqlClient.prepare('DELETE FROM userglobal WHERE userid = ? AND globalid = ?;')
    .run(userId, roomId)
}

/**
 * Link a user to a room
 * ? I think [0,0] set the rights to default: is asking for access
 * @param userId : string
 * @param roomId : string
 * @param rank : number
 * @returns {*}
 */
exports.insertUserRoomRelation = (userId, roomId, rank) => {
  return sqlClient.prepare("INSERT OR REPLACE INTO userglobal (id, userid, globalid, rank, role) VALUES (?, ?, ?, ?, ?);")
    .run(userId + roomId, userId, roomId, rank, '[0,0]')
}

/**
 *
 * @param roomId : string
 */
exports.deleteRoom = (roomId) => {
  sqlClient.prepare('DELETE FROM userglobal WHERE globalid = ?').run(roomId);
  sqlClient.prepare('DELETE FROM global WHERE id = ?').run(roomId);
}

/**
 * Find members of a room
 * @param roomId : string
 */
exports.findUsersInRoom = (roomId) => {
  return sqlClient.prepare('SELECT users.id, users.name, users.avatar, userglobal.rank, userglobal.role FROM users INNER JOIN userglobal WHERE users.id=userglobal.userid AND userglobal.globalid= ? ORDER BY (userglobal.rank) ASC;')
    .all(roomId);
}

/**
 *
 * @param toInsert : Object roomObject
 * @returns {*}
 */
exports.createRoom = (toInsert) => {
  return sqlClient.prepare("INSERT INTO global (id, admin, settings, techtree,refinery, production, storage, stockpiles,fobs, requests, misc, arty,squads,logi,events) VALUES (@id, @admin, @settings, '', '', '', '', '', '', '', '', '', @squads,  '','[]');")
    .run(toInsert)
}

/**
 *
 * @param roomId : string
 * @param userId : string
 * @returns {*}
 */
exports.setRoomAdmin = (roomId, userId) => {
  return sqlClient.prepare('UPDATE global SET admin = ? WHERE id = ?;').run(userId, roomId)
}

/**
 *
 * @param roomId : string
 * @param techTree : string
 * @returns {*}
 */
exports.updateRoomTech = (roomId, techTree) => {
  return sqlClient.prepare('UPDATE global SET techtree = ? WHERE id = ?;')
    .run(techTree, roomId)
}

/**
 *
 * @param roomId : string
 * @param misc : string
 * @returns {*}
 */
exports.updateRoomMisc = (roomId, misc) => {
  return sqlClient.prepare('UPDATE global SET misc = ? WHERE id = ?;').run(misc, roomId)
}

/**
 *
 * @param roomId : string
 * @param objectType : string
 * @param objectValue : Object
 * @returns {*}
 */
exports.updateRoomObject = (roomId, objectType, objectValue) => {
  return sqlClient.prepare(`UPDATE global SET ${objectType} = ? WHERE id = ?;`)
    .run(objectValue, roomId)
}

/**
 *
 * @param roomId : string
 * @param artyValue : string
 * @returns {*}
 */
exports.addArtyResult = function (roomId, artyValue) {
  return sqlClient.prepare('UPDATE global SET arty = ? WHERE id = ?;')
    .run(artyValue, roomId)
};

/**
 *
 * @param roomId : string
 * @param squadsValue : string
 * @returns {*}
 */
exports.updateRoomSquads = function (roomId, squadsValue) {
  return sqlClient.prepare('UPDATE global SET squads = ? WHERE id = ?;')
    .run(squadsValue, roomId)
};

/**
 *
 * @param roomId : string
 * @param eventsValue : string
 * @returns {*}
 */
exports.addRoomEvents = function (roomId, eventsValue) {
  return sqlClient.prepare('UPDATE global SET events = ? WHERE id = ?;')
    .run(eventsValue, roomId)
};

/**
 *
 * @param roomId : string
 * @param settingsValue : string
 * @returns {*}
 */
exports.addRoomSetings = function (roomId, settingsValue) {
  return sqlClient.prepare('UPDATE global SET settings = ? WHERE id = ?;')
    .run(settingsValue, roomId)
};

/**
 *
 * @param roomId
 * @returns {*}
 */
exports.deleteAnonymousLikeUser = function (roomId) {
  return sqlClient.prepare('DELETE FROM userglobal WHERE userid LIKE "anonymous%" AND globalid = ?;')
    .run(roomId)
};

/**
 *
 * @param roomId
 * @param roomAdminUserId
 * @param roomSettings
 */
exports.clearRoom = function (roomId, roomAdminUserId, roomSettings) {
  return sqlClient.prepare('INSERT OR REPLACE INTO global (id, admin, settings, techtree,refinery, production, storage, stockpiles,fobs, requests, misc, arty,squads,logi,events) VALUES (@id, @admin, @settings, "", "","", "", "","", "", "", "", "",  "","[]");')
    .run({
      id: roomId,
      admin: roomAdminUserId,
      settings: roomSettings,
    });
}

/**
 *
 * @param roomId : string
 * @param misc : string
 */
exports.clearMap = function (roomId, misc) {
  return sqlClient.prepare('UPDATE global SET fobs="",requests="",misc=?  WHERE id = ?').run(misc, roomId);
}


/**
 *
 * @param roomId : string
 * @param settings : string
 */
exports.updateRoomSettings = function (roomId, settings) {
  return sqlClient.prepare('UPDATE global SET settings = ? WHERE id = ?').run(settings, roomId)
}

