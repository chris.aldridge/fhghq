/**
 * War API Dynamic Data repository
 */
const sqlClient = require('./client')()

/**
 * Look for all events in the db
 * Limited to the 400 latests events
 * @returns {*}
 */
exports.list = () => {
  return sqlClient.prepare('SELECT * FROM apidata_static').all()
}

/**
 * Look for all events in the db
 * Limited to the 400 latests events
 * @returns {*}
 */
exports.listRegionNames = () => {
  return sqlClient.prepare('SELECT DISTINCT regionName FROM apidata_static')
    .all()
}

/**
 * @param regionName : string
 * @returns {*}
 */
exports.getEtag = (regionName) => {
  return sqlClient.prepare('SELECT regionName, etag FROM apidata_static WHERE regionName = ?;')
    .get(regionName)
}

/**
 *
 * @param regionName
 * @param regionId
 * @param data
 * @param etag
 */
exports.insert = (regionName, regionId, data, etag) => {
  return sqlClient.prepare('INSERT OR REPLACE INTO apidata_static (regionName, regionId, data, etag) VALUES (@regionName, @regionId, @data, @etag);')
    .run({
      regionName,
      regionId,
      data,
      etag
    })
}
