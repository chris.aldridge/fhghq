/**
 * UserRepository
 *
 * model:
 *
 * export interface User {
    id: string,
    salt: string,
    name: string,
    avatar: string
  }
 */
const sqlClient = require('./client')()

exports.getUserById = (id) => {
  return sqlClient.prepare('SELECT * FROM users WHERE id= ?;')
    .get(id)
}

exports.createUser = (user) => {
  return sqlClient.prepare('INSERT INTO users (id, salt, name, avatar) VALUES (?, ?, ?, ?);')
    .run(user.id, user.salt, user.name, user.avatar)
}

exports.updateUser = (user) => {
  return sqlClient.prepare('UPDATE users SET name = ?, avatar = ? WHERE id = ?;')
    .run(user.name, user.avatar, user.id)
}

/**
 *
 * @param userId
 * @param roomId
 * @param rank
 * @returns {*}
 */
exports.updateUserRankInRoom = (userId, roomId, rank) => {
  return sqlClient.prepare('UPDATE userglobal SET rank = ? WHERE userid = ? AND globalid =?;')
    .run(rank, userId, roomId)
}

/**
 *
 * @param userId : string
 * @param roomId : string
 * @param role : string
 * @returns {*}
 */
exports.updateUserRoleInRoom = (userId, roomId, role) => {
  return sqlClient.prepare('UPDATE userglobal SET role = ? WHERE userid = ? AND globalid =?;')
    .run(role, userId, roomId)
}

/**
 *
 * @param roomId
 * @returns {*}
 */
exports.getRoomArty = (roomId) => {
  return sqlClient.prepare('SELECT arty FROM global WHERE id = ?')
    .get(roomId)
}

