FROM node:12.22

WORKDIR /opt/fhghq

RUN mkdir .data

COPY ["package.json", "./"]
RUN npm install

ADD conf conf
ADD src src
ADD views views
ADD server server

COPY [ ".babelrc", "webpack.config.js", "./" ]

COPY [ "0.26 Town List.md", "./" ]
RUN npm run-script build

ENTRYPOINT ["npm", "start"]
